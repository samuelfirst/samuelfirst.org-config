# samuelfirst.org-config

Start the docker containers:
`docker compose up -d`

The nginx config is stored in the docker volume located at:
`/var/lib/docker/volumes/samuelfirstorg-config_nginx_conf/_data/`


Certbot can be automated like this:
```
docker run -it --rm --name certbot \
       -v "etc_letsencrypt:/etc/letsencrypt:rw" \
	   -v "var_lib_letsencrypt:/var/lib/letsencrypt:rw" \
	   -v "nginx_conf:/etc/nginx" \
	   --mount type=bind,source=~/content,destination=/usr/share/nginx/html\
	   certbot/certbot --nginx
	   
docker run -it --rm --name certbot \
       -v "etc_letsencrypt:/etc/letsencrypt:rw" \
	   -v "var_lib_letsencrypt:/var/lib/letsencrypt:rw" \
	   -v "nginx_conf:/etc/nginx" \
	   --mount type=bind,source=~/content,destination=/usr/share/nginx/html\
	   certbot/certbot renew
```
