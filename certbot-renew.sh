#!/bin/bash

docker exec -i $(docker ps | grep nginx-certbot | awk '{print $1}') \
		 certbot renew
