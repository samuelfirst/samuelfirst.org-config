#!/bin/bash

docker exec -i $(docker ps | grep nginx-certbot | awk '{print $1}') \
		 certbot -n -m samuelfirst@protonmail.com --agree-tos \
		 	 --nginx -d samuelfirst.org
